<?php

namespace App\Controller;

use DateTime;
use App\Entity\Todo;
use App\Repository\TodoRepository;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class TodoController extends AbstractController
{
    #[Route('/todo', name: 'app_todo')]
    public function index(TodoRepository $todos): Response
    {
        return $this->render('todo/index.html.twig', [
            'todos' => $todos->findAll(),
        ]);
    }

    #[Route('/todo/{todo}', name: 'app_todo_show')]
    public function showOne(Todo $todo): Response
    {
        return $this->render('todo/show.html.twig', [
            'todo' => $todo,
        ]);
    }

    #[Route('/todo/add', name: 'app_todo_add', priority: 2)]
    #[IsGranted('ROLE_USER')]
    public function add(Request $request,  TodoRepository $todos): Response
    {
        $todo = new Todo();
        $form = $this->createFormBuilder($todo)
            ->add('title')
            //->add('subit', SubmitType::class, ['label' => 'Zapisz'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();
            $todo->setCreated(new DateTime());

            $todos->add($todo, true);

            $this->addFlash('success', 'Twoja lista została utworzona');

            return $this->redirectToRoute('app_todo');
        }

        return $this->renderForm(
            'todo/add.html.twig',
            [
                'form' => $form
            ]
        );
    }

    #[Route('/todo/{todo}/edit', name: 'app_todo_edit')]
    public function edit(Todo $todo, Request $request,  TodoRepository $todos): Response
    {
        $form = $this->createFormBuilder($todo)
            ->add('title')
            //->add('subit', SubmitType::class, ['label' => 'Zapisz'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();
            $todos->add($todo, true);

            $this->addFlash('success', 'Twoja lista zaaktualizowana');

            return $this->redirectToRoute('app_todo');
        }

        return $this->renderForm(
            'todo/add.html.twig',
            [
                'form' => $form
            ]
        );
    }
}
