<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Todo;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher
    ) {
    }
    
    public function load(ObjectManager $manager): void
    {

        $user1 = new User();
        $user1->setEmail('test@test.com');
        $user1->setPassword(
            $this->userPasswordHasher->hashPassword(
                $user1,
                'test123'
            )
        );
        $manager->persist($user1);

        $todo1 = new Todo();
        $todo1 -> setTitle('Pierwsza lista');
        $todo1 -> setCreated(new DateTime());
        $manager->persist($todo1);

        $todo2 = new Todo();
        $todo2 -> setTitle('Druga lista');
        $todo2 -> setCreated(new DateTime());
        $manager->persist($todo2);

        $todo3 = new Todo();
        $todo3 -> setTitle('Trzecia lista');
        $todo3 -> setCreated(new DateTime());
        $manager->persist($todo3);

        $manager->flush();
    }
}
